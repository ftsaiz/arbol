package com.ibermatica.arbol.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.ibermatica.arbol.beans.EstacionType;
import com.ibermatica.arbol.serviceImpl.EstacionServiceImpl;

/**
 * Clase de test de la capa service
 * @author MASAMAFT
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class EstacionServiceUnitTest {
	
	@InjectMocks
	private EstacionService estacionService = new EstacionServiceImpl();
	
	@Test
	public void dadoUnaEstacionYNumeroDeCiclosCuandoRealizamosLaConsultaEntoncesNoDevuelveNulo() throws Exception {
		int numCiclos = 3;
		assertNotNull(estacionService.getMetros(EstacionType.INVIERNO, numCiclos));
	}

	@Test
	public void dadoEstacionOtoñoYCincoCiclosCuandoRealizamosLaConsultaEntoncesDevuelveUnMetro() throws Exception {
		int numCiclos = 5;
		int resultadoMetros = 1;
		assertEquals(resultadoMetros, estacionService.getMetros(EstacionType.OTOÑO, numCiclos));
	}
	
	@Test
	public void dadoEstacionOtoñoYTreceCiclosCuandoRealizamosLaConsultaEntoncesDevuelveSieteMetros() throws Exception {
		int numCiclos = 13;
		int resultadoMetros = 7;
		assertEquals(resultadoMetros, estacionService.getMetros(EstacionType.OTOÑO, numCiclos));
	}
	
	@Test
	public void dadoEstacionVeranoYTreceCiclosCuandoRealizamosLaConsultaEntoncesDevuelveCatorceMetros() throws Exception {
		int numCiclos = 13;
		int resultadoMetros = 14;
		assertEquals(resultadoMetros, estacionService.getMetros(EstacionType.PRIMAVERA, numCiclos));
	}
	
	@Test	
	public void dadoEstacionVeranoYNueveCiclosCuandoRealizamosLaConsultaEntoncesDevuelveSieteMetros() throws Exception {
		int numCiclos = 9;
		int resultadoMetros = 7;
		assertEquals(resultadoMetros, estacionService.getMetros(EstacionType.VERANO, numCiclos));
	}
	

}
