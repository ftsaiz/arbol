package com.ibermatica.arbol.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.ui.Model;

import com.ibermatica.arbol.beans.Estacion;
import com.ibermatica.arbol.beans.EstacionType;
import com.ibermatica.arbol.service.EstacionService;


/**
 * Clase de test para probar el controlador EstacionController
 * @author MASAMAFT
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class EstacionControllerUnitTest {
	
	@Mock
	private EstacionService estacionService;
	
	@Mock
	private Model model;
	
	@Mock
	private Estacion estacion;	
	
	@InjectMocks
	private EstacionController estacionController;
	
	@Test
	public void dadoNingunParametroCuandoSolicitamosPantallaInicialSeNosDevuelvePantallaInicial() throws Exception {
		estacionController.index(model);
		verify(model).addAttribute("estacion", new Estacion());		
	}

	
	@Test
	public void dadoUnaEstacionYNumeroDeCiclosCuandoRealizamosConsultaInvocaServicioMetros() throws Exception {
		when(estacion.getTipoEstacion()).thenReturn(EstacionType.INVIERNO);		
		estacionController.index(estacion, 3, model);
		verify(estacionService).getMetros(EstacionType.INVIERNO, 3);
	}

	
	@Test
	public void dadoUnaEstacionNulaYNumeroDeCiclosCuandoRealizamosConsultaSeNosDevuelvePantallaDeError() throws Exception {
		when(estacion.getTipoEstacion()).thenReturn(null);	
		String resutlado = estacionController.index(estacion, 3, model);		
		assertEquals("error", resutlado);	
	}

	
	@Test
	public void dadoUnaEstacionYNumeroDeCiclosCeroCuandoRealizamosConsultaSeNosDevuelvePantallaDeError() throws Exception {
		when(estacion.getTipoEstacion()).thenReturn(EstacionType.INVIERNO);	
		String resutlado = estacionController.index(estacion, 0, model);		
		assertEquals("error", resutlado);
		
	}
}
