package com.ibermatica.arbol.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class) 
@SpringBootTest
@AutoConfigureMockMvc
public class EstacionControllerIT {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void dadoNingunParametroCuandoSolicitamosPantallaInicialSeNosDevuelvePantallaInicial() throws Exception {
		mockMvc.perform(
				get("/index"))
				  .andExpect(status().is2xxSuccessful());
	}
	
	 	
	@Test
	public void dadoUnaEstacionYNumeroDeCiclosCuandoRealizamosConsultaInvocaServicioMetros() throws Exception {		
		mockMvc.perform(
				post("/index")
				.param("estacionInicio", "PRIMAVERA")
				.param("numeroCiclos", "10"))
				.andExpect(status().is2xxSuccessful());
	}
	
}
