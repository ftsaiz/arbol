package com.ibermatica.arbol.beans;

/**
 * Clase de estación del tiempo
 * @author MASAMAFT
 *
 */
public class Estacion {
	
	private String crecimiento;
	private EstacionType tipoEstacion;
	
	public Estacion () {
		
	}
			
	public Estacion (String sCrecimiento, EstacionType tEstacion) {
		this.crecimiento = sCrecimiento;
		this.tipoEstacion = tEstacion;
	}

	public String getCrecimiento() {
		return crecimiento;
	}

	public void setCrecimiento(String crecimiento) {
		this.crecimiento = crecimiento;
	}

	public EstacionType getTipoEstacion() {
		return tipoEstacion;
	}

	public void setTipoEstacion(EstacionType tipoEstacion) {
		this.tipoEstacion = tipoEstacion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((crecimiento == null) ? 0 : crecimiento.hashCode());
		result = prime * result + ((tipoEstacion == null) ? 0 : tipoEstacion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estacion other = (Estacion) obj;
		if (crecimiento == null) {
			if (other.crecimiento != null)
				return false;
		} else if (!crecimiento.equals(other.crecimiento))
			return false;
		if (tipoEstacion != other.tipoEstacion)
			return false;
		return true;
	}
	
	
	
	
}
