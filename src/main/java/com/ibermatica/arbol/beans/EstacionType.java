package com.ibermatica.arbol.beans;

import java.util.HashMap;
import java.util.Map;

/**
 * Tipo de estación
 * @author MASAMAFT
 *
 */
public enum EstacionType {
	PRIMAVERA(0),
    VERANO(1),
    OTOÑO(2),
    INVIERNO(3),
    ;

    private int codEstacion;
    private static Map<Object, Object> map = new HashMap<>();

    EstacionType(int codEstacion) {
        this.codEstacion = codEstacion;
    }

    public int getCodEstacion(){
        return codEstacion;
    };
    
    static {
        for (EstacionType estacionType : EstacionType.values()) {
            map.put(estacionType.codEstacion, estacionType);
        }
    }

    public static EstacionType valueOf(int estacionType) {
        return (EstacionType) map.get(estacionType);
    }
}
