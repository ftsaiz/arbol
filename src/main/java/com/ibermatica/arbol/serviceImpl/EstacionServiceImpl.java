package com.ibermatica.arbol.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.ibermatica.arbol.beans.Estacion;
import com.ibermatica.arbol.beans.EstacionType;
import com.ibermatica.arbol.service.EstacionService;

/**
 * Implementación de la clase estacion
 * @author MASAMAFT
 *
 */
@Service
public class EstacionServiceImpl implements EstacionService {
	
	private static Logger logger = LoggerFactory.getLogger(EstacionServiceImpl.class);

	@Override
	public int getMetros(EstacionType tipoEstacion, int numCiclos) {
		
		List<Estacion> lstEstaciones = new ArrayList<Estacion>();
		
		lstEstaciones.add(new Estacion("*2", EstacionType.PRIMAVERA));
		lstEstaciones.add(new Estacion("+1", EstacionType.VERANO));
		lstEstaciones.add(new Estacion("+0", EstacionType.OTOÑO));
		lstEstaciones.add(new Estacion("+0", EstacionType.INVIERNO));
		
		int metros = 0;
		String crecimiento = "";
		int numInicioEstacion = tipoEstacion.getCodEstacion();
		int numCiclosRecorrer = numInicioEstacion + numCiclos;
		
		for (int i=numInicioEstacion; i<numCiclosRecorrer; i++) {
			
			crecimiento = lstEstaciones.get(calculaCiclo(i)).getCrecimiento();
			metros = evaluarOperacion(metros, crecimiento);
			
			/*logger.debug("iteracion: " + i);
			logger.debug("crecimiento: " + crecimiento);
			logger.debug("metros: " + metros);
			logger.debug("-----------");*/
		}		
				
		return metros;
	}

	
	/**
	 * Devuelve la estación dado una estación de inicio, teniendo el cuenta el ciclo primavera-0, verano-1, otoño-2, invierno-3
	 * @param inicioEstacion
	 * @return
	 */
	private int calculaCiclo(int numCiclo) {
		int resultado = numCiclo % EstacionType.values().length;
		return resultado;
	}
	
	
	/**
	 * Método para evaluar una operacion matemática
	 * @param numero
	 * @param operacion
	 * @return
	 */
	private int evaluarOperacion(int numero, String operacion) {
		ScriptEngineManager mgr = new ScriptEngineManager();
	    ScriptEngine engine = mgr.getEngineByName("JavaScript");
	    //int numero = 3;
	    //String operacion = "*2";
	    int nResult = 0;		
	    try {
	    	String operacionCompleta = String.valueOf(numero)+operacion;
	    	nResult = (int) engine.eval(operacionCompleta);
	    	//logger.debug("operacionCompleta resultado: " + operacionCompleta + "=" + nResult);
		} catch (ScriptException e) {
			logger.error("Error en EstacionServiceImpl.evaluarOperacion", e);
		}
	    return nResult;
	}

}
