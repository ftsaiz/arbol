package com.ibermatica.arbol.service;

import com.ibermatica.arbol.beans.EstacionType;

/**
 * Clase Interfaz del Servicio estacion
 * @author MASAMAFT
 *
 */
public interface EstacionService {

	/**
	 * Devuelve el número de metros de un árbol 
	 * @return
	 */
	public int getMetros(EstacionType tipoEstacion, int numCiclos);
	
	/**
	 * Devuelve la estación dado una estación de inicio, teniendo el cuenta el ciclo primavera-0, verano-1, otoño-2, invierno-3
	 * @param inicioEstacion
	 * @return
	 */
	//public int calculaCiclo(int inicioEstacion);
}
