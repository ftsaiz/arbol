package com.ibermatica.arbol.controller;

import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ibermatica.arbol.beans.Estacion;
import com.ibermatica.arbol.beans.EstacionType;
import com.ibermatica.arbol.service.EstacionService;

/**
 * Clase de la capa controller de estación
 * @author MASAMAFT
 *
 */
@Controller
public class EstacionController {
	
	private static Logger logger = LoggerFactory.getLogger(EstacionController.class);
	
	
	@Autowired
	private EstacionService estacionService;
	
	
	//@GetMapping(value ="/index")
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index(Model model) {
		
		List<Estacion> lstEstaciones = new ArrayList<Estacion>();
		
		lstEstaciones.add(new Estacion("", EstacionType.PRIMAVERA));
		lstEstaciones.add(new Estacion("", EstacionType.VERANO));
		lstEstaciones.add(new Estacion("", EstacionType.OTOÑO));
		lstEstaciones.add(new Estacion("", EstacionType.INVIERNO));
		
		model.addAttribute("estacion", new Estacion());
		model.addAttribute("lstEstaciones", lstEstaciones);
		return "index";
	}

	
	@PostMapping(value ="/index")
	public String index(Estacion estacion, @RequestParam(value = "numeroCiclos", required = true) int numeroCiclos, Model model) {
		
		
		if (estacion.getTipoEstacion() == null) {
			model.addAttribute("motivoError", "Es necesario seleccionar un tipo de estacion");
			return "error";
		}
		if (numeroCiclos <= 0) {
			model.addAttribute("motivoError", "Es necesario rellenar número de ciclos");
			return "error";
		}
		int metros = estacionService.getMetros(EstacionType.valueOf(estacion.getTipoEstacion().getCodEstacion()), numeroCiclos);
		
		model.addAttribute("metros", metros);
				
		
		return "index";
	}
	


}
